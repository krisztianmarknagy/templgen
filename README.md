
# Template Generator

## Flags

```txt
-t or --t stands for `template`:
    The value must be a path to your desired template.

-d or --d stands for `destination`:
    A path that points where you want your project to be.

-n or --n stands for `name`:
    String that represents the name of your project.

-git or --git stands for `git` (version control):
    As long as this flag is present as one of your arguments, it will initialize a local git repository for your project.
```

## Installation

### Execute the following commands

```bash
make install
```


