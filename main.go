package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

var (
	template     string
	destination  string
	project_name string
	gitinit      bool

	template_name          string
	project_path           string
	unrenamed_project_path string
)

func init() {
	flag.StringVar(&template, "t", "", "Path to the template of your choosing")
	flag.StringVar(&destination, "d", ".", "Path to the template's destination")
	flag.StringVar(&project_name, "n", "placeholder_project_name", "Project Name")
	flag.BoolVar(&gitinit, "git", false, "Initializes git if true (default \"false\")")
}

func main() {
	flag.Parse()

	_tmp := strings.Split(strings.TrimRight(template, "/"), "/")
	template_name = _tmp[len(_tmp)-1]

	unrenamed_project_path = destination + string(os.PathSeparator) + template_name
	project_path = destination + string(os.PathSeparator) + project_name

	if template == "" {
		fmt.Println("[Error] `-t`/`--t` flag is required; flag's value must be a valid path")
		return
	}

	fmt.Printf("Template Name: %s\n", template_name)

	if err := exec.Command("cp", strings.Split(fmt.Sprintf("-r::-v::%s::%s", template, destination), "::")...).Run(); err != nil {
		fmt.Printf("[Error] `exec.Command(\"cp -r -v %s %s\")`; error: %v\nQuitting Program...\n", template_name, destination, err.Error())
		return
	}

	if err := os.Rename(unrenamed_project_path, project_path); err != nil {
		fmt.Printf("[Error] `os.Rename(\"%s\", \"%s\")`; error: %v\nQuitting Program...\n", unrenamed_project_path, project_path, err.Error())
		return
	}

	if err := os.Chdir(project_path); err != nil {
		fmt.Printf("[Error] `os.Chdir(\"%s\")`; error: %v\nQuitting Program...\n", project_path, err.Error())
		return
	}

	cwd, err := os.Getwd()
	if err != nil {
		fmt.Printf("[Error] `os.Getwd()`; error: %v\nQuitting Program...\n", err.Error())
		return
	}

	if gitinit {
		fmt.Printf("[Info] Initializing Git inside '%s'\n", cwd)
		if err := exec.Command("git", "init").Run(); err != nil {
			fmt.Printf("[Error] `exec.Command(\"git\", \"init\")`; error: %v\nQuitting Program...\n", err.Error())
			return
		}
		fmt.Println("[Info] Git has been successfully initialized")
	}

	fmt.Printf("[Success] Your project '%s' is up for hacking!\nPath to your project: %s\n", project_name, cwd)
}
